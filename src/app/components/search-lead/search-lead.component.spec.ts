import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchLeadComponent } from './search-lead.component';

describe('SearchLeadComponent', () => {
  let component: SearchLeadComponent;
  let fixture: ComponentFixture<SearchLeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchLeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
