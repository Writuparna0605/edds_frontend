import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIndividualContactComponent } from './add-individual-contact.component';

describe('AddIndividualContactComponent', () => {
  let component: AddIndividualContactComponent;
  let fixture: ComponentFixture<AddIndividualContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIndividualContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIndividualContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
