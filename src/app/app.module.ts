import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { LeftmenuComponent } from './components/leftmenu/leftmenu.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { ContactManagementComponent } from './components/contact-management/contact-management.component';
import { LeadManagementComponent } from './components/lead-management/lead-management.component';
import { SearchContactComponent } from './components/search-contact/search-contact.component';
import { AddIndividualContactComponent } from './components/add-individual-contact/add-individual-contact.component';
import { AddLeadComponent } from './components/add-lead/add-lead.component';
import { SearchLeadComponent } from './components/search-lead/search-lead.component';
import { SearchSectionComponent } from './components/search-section/search-section.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LeftmenuComponent,
    DashboardComponent,
    HeaderComponent,
    ContactManagementComponent,
    LeadManagementComponent,
    SearchContactComponent,
    AddIndividualContactComponent,
    AddLeadComponent,
    SearchLeadComponent,
    SearchSectionComponent,
    BreadcrumbComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
