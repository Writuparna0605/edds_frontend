import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent } from './components/login/login.component';
import {DashboardComponent } from './components/dashboard/dashboard.component';
import {LeadManagementComponent } from './components/lead-management/lead-management.component';
import {ContactManagementComponent } from './components/contact-management/contact-management.component';
import {SearchContactComponent } from './components/search-contact/search-contact.component';
import {AddIndividualContactComponent } from './components/add-individual-contact/add-individual-contact.component';
import {AddLeadComponent } from './components/add-lead/add-lead.component';
import {SearchLeadComponent } from './components/search-lead/search-lead.component';

const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'lead-management',
        component: LeadManagementComponent
    },
    {
        path: 'add-lead',
        component: AddLeadComponent
    },
    {
        path: 'search-lead',
        component: SearchLeadComponent
    },
    {
        path: 'contact-management',
        component: ContactManagementComponent
    },
    {
        path: 'search-contact',
        component: SearchContactComponent
    },
    {
        path: 'add-individual-contact',
        component: AddIndividualContactComponent
    },
    {
      path: '**',
      component:LoginComponent,
      pathMatch: 'full'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
