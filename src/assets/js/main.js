$(document).ready(function(){
$('main').addClass('squezeContent');
$('#show_hide_menu').addClass('showDiv');
  $('#show_hide_menu').click(function(){
    if($('#left_bar').hasClass('showDiv') && $('#show_hide_menu').hasClass('showDiv')){
      $('#left_bar').removeClass('showDiv');
      $('#show_hide_menu').removeClass('showDiv');
      $('#left_bar').addClass('hideDiv');
      $('#show_hide_menu').addClass('hideDiv');
      $('main').removeClass('squezeContent');
    }else{
      $('#left_bar').addClass('showDiv');
      $('#left_bar').removeClass('hideDiv');
      $('#show_hide_menu').addClass('showDiv');
      $('#show_hide_menu').removeClass('hideDiv');
      $('main').addClass('squezeContent');
    }
  });
  $('#openSearch').click(function(){
    if($('#search_form_sec').hasClass('showDiv')){
      $('#search_form_sec').removeClass('showDiv');
      $('#openSearch').removeClass('fa-angle-up');
      $('#openSearch').addClass('fa-angle-down');
      $('#search_form_sec').addClass('hideDiv');
    }else{
      $('#search_form_sec').addClass('showDiv');
      $('#search_form_sec').removeClass('hideDiv');
      $('#openSearch').addClass('fa-angle-up');
      $('#openSearch').removeClass('fa-angle-down');
    }
  });
  $('#openlogin').click(function(){
    console.log('test');
    if($('#top_login_sec').hasClass('showDiv')){
      $('#top_login_sec').removeClass('showDiv');
      $('#openlogin').removeClass('fa-angle-up');
      $('#openlogin').addClass('fa-angle-down');
      $('#top_login_sec').addClass('hideDiv');
    }else{
      $('#top_login_sec').addClass('showDiv');
      $('#top_login_sec').removeClass('hideDiv');
      $('#openlogin').addClass('fa-angle-up');
      $('#openlogin').removeClass('fa-angle-down');
    }
  });
  $('#openContactSearch').click(function(){
    if($('#search_form_contact').hasClass('showDiv')){
      $('#search_form_contact').removeClass('showDiv');
      $('#search_form_contact').addClass('hideDiv');
    }else{
      $('#search_form_contact').addClass('showDiv');
      $('#search_form_contact').removeClass('hideDiv');
    }
  });
  $('#dashboard_table_value').find('input').css("display","none");
  $('.editTable').click(function(){
    $(this).parent().parent().find('p').css("display","none");
      $(this).parent().parent().find('input').css("display","block");
      $(this).parent().parent().addClass('selected');
 });
 $('.saveTable').click(function(){
   $(this).parent().parent().removeClass('selected');
     $(this).parent().parent().find('p').css("display","block");
       $(this).parent().parent().find('input').css("display","none");

 });
 var select_all = document.getElementById("selectAll"); //select all checkbox
 var checkboxes = document.getElementsByClassName("selectIndividual"); //checkbox items
 $('.toggle').click(function(e) {
   	e.preventDefault();
     var $this = $(this);
     if ($this.find('i.fas ').hasClass('fa-angle-down')) {
         $this.find('i.fas ').removeClass('fa-angle-down');
         $this.find('i.fas ').addClass('fa-angle-up');
     }else{
         $this.find('i.fas ').removeClass('fa-angle-up');
         $this.find('i.fas ').addClass('fa-angle-down');
     }
     if ($this.next().hasClass('show')) {
         $this.next().removeClass('show');
         $this.next().slideUp(350);
     } else {
         $this.parent().parent().find('li .inner').removeClass('show');
         $this.parent().parent().find('li .inner').slideUp(350);
         $this.next().toggleClass('show');
         $this.next().slideToggle(350);
     }
 });
 //select all checkboxes
 // select_all.addEventListener("change", function(e){
 //     for (i = 0; i < checkboxes.length; i++) {
 //         checkboxes[i].checked = select_all.checked;
 //     }
 // });
 //
 //
 // for (var i = 0; i < checkboxes.length; i++) {
 //     checkboxes[i].addEventListener('change', function(e){
 //         if(this.checked == false){
 //             select_all.checked = false;
 //         }
 //         if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
 //             select_all.checked = true;
 //         }
 //     });
 // }




})
